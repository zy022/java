//
//
package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 角色查询实体
 * </p>
 *
 @author ty
 @create 2023-03-06-23:31
 */

@Data
@ApiModel("角色查询实体")
public class SysRoleQueryVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("角色名称")
	private String roleName;
}

