package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 @author ty
 @create 2023-03-06-23:31
 */

@Data
@ApiModel("岗位查询实体")
public class SysPostQueryVo {

	@ApiModelProperty(value = "岗位编码")
	private String postCode;

	@ApiModelProperty(value = "岗位名称")
	private String name;

	@ApiModelProperty(value = "状态（1正常 0停用）")
	private Boolean status;


}

