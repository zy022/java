package com.ty.ssm.model.base;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 @author ty
 @create 2023-03-02-22:35  
 */

@Data
@Accessors(chain = true)
public class BaseEntity implements Serializable {
	@TableId(value = "id", type = IdType.AUTO)
	private String id;

	@TableField(value = "create_time", fill = FieldFill.INSERT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;

	@TableLogic  //逻辑删除 默认效果 0 没有删除 1 已经删除
	@TableField("is_deleted")
	private Integer isDeleted;

	@TableField(exist = false)
	private Map<String, Object> param = new HashMap<>();
}
