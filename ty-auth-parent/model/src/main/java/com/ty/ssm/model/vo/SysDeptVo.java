package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("用户查询实体")
public class SysDeptVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("关键词")
    private String keyword;

    @ApiModelProperty("开始创建时间")
    private String createTimeBegin;

    @ApiModelProperty("结束创建时间")
    private String createTimeEnd;

    @ApiModelProperty("角色id")
    private Long roleId;

    @ApiModelProperty("岗位Id")
    private Long postId;

    @ApiModelProperty("部门id")
    private Long deptId;

}

