package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 @author ty
 @create 2023-03-06-23:31
 */

@Data
@ApiModel("登录信息查询实体")
public class SysLoginLogQueryVo {

	@ApiModelProperty(value = "用户账号")
	private String username;

	@ApiModelProperty("开始创建时间")
	private String createTimeBegin;

	@ApiModelProperty("结束创建时间")
	private String createTimeEnd;

}

