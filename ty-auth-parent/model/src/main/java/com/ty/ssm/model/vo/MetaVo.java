package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 @author ty
 @create 2023-03-06-23:29  
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("")
public class MetaVo {
	/**
	 * 设置该路由在侧边栏和面包屑中展示的名字
	 */
	@ApiModelProperty("设置该路由在侧边栏和面包屑中展示的名字")
	private String title;

	/**
	 * 设置该路由的图标，对应路径src/assets/icons/svg
	 */
	@ApiModelProperty("设置该路由的图标")
	private String icon;
}
