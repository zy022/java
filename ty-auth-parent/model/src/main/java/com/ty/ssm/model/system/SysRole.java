package com.ty.ssm.model.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ty.ssm.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 @author ty
 @create 2023-03-02-22:45
 */

@Data
@TableName("sys_role")
@ApiModel("角色")
public class SysRole extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@TableField("role_name")
	@ApiModelProperty("角色姓名")
	private String roleName;

	@TableField("role_code")
	@ApiModelProperty("角色编码")
	private String roleCode;

	@TableField("description")
	@ApiModelProperty("角色描述")
	private String description;

}

