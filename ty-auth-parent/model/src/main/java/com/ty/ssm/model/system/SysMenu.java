package com.ty.ssm.model.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ty.ssm.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 @author ty
 @create 2023-03-02-23:11  
 */

@Data
@ApiModel(description = "菜单")
@TableName("sys_menu")
public class SysMenu extends BaseEntity {

	@TableField("parent_id")
	@ApiModelProperty(value = "所属上级")
	private String parentId;

	@TableField("name")
	@ApiModelProperty(value = "名称")
	private String name;

	@ApiModelProperty(value = "类型(1:菜单,2:按钮)")
	@TableField("type")
	private Integer type;

	@TableField("path")
	@ApiModelProperty(value = "路由地址")
	private String path;

	@TableField("component")
	@ApiModelProperty(value = "组件路径")
	private String component;

	@TableField("perms")
	@ApiModelProperty(value = "权限标识")
	private String perms;

	@TableField("icon")
	@ApiModelProperty(value = "图标")
	private String icon;

	@TableField("sort_value")
	@ApiModelProperty(value = "排序")
	private Integer sortValue;

	@ApiModelProperty(value = "状态（1正常 0停用）")
	@TableField("status")
	private Integer status;

	@ApiModelProperty(value = "下级菜单")
	@TableField(exist = false)
	private List<SysMenu> children;

	//是否选中
	@TableField(exist = false)
	private boolean isSelect;
}
