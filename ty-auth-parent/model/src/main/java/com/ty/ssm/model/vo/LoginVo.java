package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 @author ty
 @create 2023-03-06-23:27
 登录对象
 */

@Data
@ApiModel("登录对象")
public class LoginVo {

	@ApiModelProperty(value = "用户名")
	private String username;

	/**
	 * 密码
	 */
	@ApiModelProperty(value = "密码")
	private String password;
}
