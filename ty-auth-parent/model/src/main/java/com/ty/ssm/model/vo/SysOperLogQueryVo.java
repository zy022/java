package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 @author ty
 @create 2023-03-06-23:31
 */

@Data
@ApiModel("操作模块查询实体")
public class SysOperLogQueryVo {

	@ApiModelProperty("模块标题")
	private String title;

	@ApiModelProperty("操作名称")
	private String operName;

	@ApiModelProperty("开始创建时间")
	private String createTimeBegin;

	@ApiModelProperty("结束创建时间")
	private String createTimeEnd;

}

