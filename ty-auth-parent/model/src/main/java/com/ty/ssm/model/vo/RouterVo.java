package com.ty.ssm.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 @author ty
 @create 2023-03-06-23:31  
 */

@Data
@ApiModel("路由配置信息")
public class RouterVo {
	/**
	 * 路由地址
	 */
	@ApiModelProperty("路由地址")
	private String path;

	/**
	 * 是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现
	 */
	@ApiModelProperty("是否隐藏路由，当设置 true 的时候该路由不会再侧边栏出现")
	private boolean hidden;

	/**
	 * 组件地址
	 */
	@ApiModelProperty("组件地址")
	private String component;

	/**
	 * 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
	 */
	@ApiModelProperty("当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面")
	private Boolean alwaysShow;

	/**
	 * 其他元素
	 */
	@ApiModelProperty("其他元素")
	private MetaVo meta;

	/**
	 * 子路由
	 */
	@ApiModelProperty("子路由")
	private List<RouterVo> children;
}
