package com.ty.ssm.model.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ty.ssm.model.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 @author ty
 @create 2023-03-02-23:06  
 */

@Data
@TableName("sys_login_log")
@ApiModel(description = "登录日志")
public class SysLoginLog extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@TableField("username")
	@ApiModelProperty("用户账号")
	private String userName;

	@TableField("ipaddr")
	@ApiModelProperty("登录IP地址")
	private String ipAddress;

	@TableField("status")
	@ApiModelProperty("登录状态（0成功 1失败）")
	private Integer status;

	@TableField("msg")
	@ApiModelProperty("提示信息")
	private String msg;

	@TableField("access_time")
	@ApiModelProperty("访问时间")
	private Date accessTime;
}
