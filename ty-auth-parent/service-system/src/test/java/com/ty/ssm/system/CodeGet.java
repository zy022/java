package com.ty.ssm.system;


import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;


public class CodeGet {

	public static void main(String[] args) {

		FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/ty-auth?characterEncoding=utf8&userSSL=false",
				"javastudy", "ty19990526")
				.globalConfig(builder -> {
					builder.author("ty") // 设置作者
							//.enableSwagger() // 开启 swagger 模式
							.fileOverride() // 覆盖已生成文件
							// 指定输出目录
							.outputDir("F:\\javaStudy\\javaproject\\ssm- " +
									"PermissionManagement\\ty-auth-parent\\service-system\\src\\main\\java");

				})
				.packageConfig(builder -> {
					builder.parent("com.ty.ssm") // 设置父包名
							.moduleName("system") // 设置父包模块名
							.pathInfo(Collections.singletonMap(OutputFile.mapperXml, "F:\\javaStudy\\javaproject" +
									"\\ssm" +
									"-" +
									" PermissionManagement\\ty-auth-parent\\service-system\\src\\main\\resources" +
									"\\Mapper")); // 设置mapperXml生成路径
				})
				.strategyConfig(builder -> {
					builder.addInclude("sys_role_menu"); // 设置需要生成的表名
				})
				.templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
				.execute();
	}
}
