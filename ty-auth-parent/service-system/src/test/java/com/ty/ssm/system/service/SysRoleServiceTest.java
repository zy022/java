package com.ty.ssm.system.service;

import com.ty.ssm.model.system.SysRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 @author ty
 @create 2023-03-07-23:11  
 */

@SpringBootTest
public class SysRoleServiceTest {
	@Autowired
	private SysRoleService sysRoleService;

	@Test
	public void testSelectAll() {
		//service实现
		List<SysRole> list = sysRoleService.list();
		System.out.println(list);
	}
}
