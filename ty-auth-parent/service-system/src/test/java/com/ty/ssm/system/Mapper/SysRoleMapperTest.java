package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ty.ssm.model.system.SysRole;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 @author ty
 @create 2023-03-06-22:54  
 */

@SpringBootTest
public class SysRoleMapperTest {

	@Autowired
	private SysRoleMapper sysRoleMapper;

	/**
	 * 1、查询表中所有记录
	 */
	@Test
	public void selectRole() {
		List<SysRole> sysRoles = sysRoleMapper.selectList(null);
		for (SysRole sysRole : sysRoles) {
			System.out.println(sysRole);
		}
	}

	/**
	 * 添加角色测试
	 */
	@Test
	public void addRole() {
		SysRole sysrole = new SysRole();

		sysrole.setRoleName("测试角色");
		sysrole.setRoleCode("testRole");
		sysrole.setDescription("测试角色");
		sysrole.setCreateTime(new Date());
		int row = sysRoleMapper.insert(sysrole);
		System.out.println(row);
	}

	/**
	 * 修改角色测试
	 */
	@Test
	public void updateRole() {
		//查询
		SysRole sysRole = sysRoleMapper.selectById(1);

		//修改值
		sysRole.setDescription("管理员");

		//修改数据
		int update = sysRoleMapper.updateById(sysRole);

		System.out.println(update);
	}

	/**
	 * 删除角色测试
	 */
	@Test
	public void removeRoleByIdTest() {
		int i = sysRoleMapper.deleteById(9);
		System.out.println(i);
	}

	/**
	 * 删除角色测试
	 */
	@Test
	public void batchRemoveRoleTest() {
		int result = sysRoleMapper.deleteBatchIds(Arrays.asList(1, 2));
		System.out.println(result);
	}

	/**
	 * 条件查询
	 */
	@Test
	public void testSelect() {
		//创建条件构造器
		QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
		sysRoleQueryWrapper.like("role_name", "管理员");
		List<SysRole> sysRoles = sysRoleMapper.selectList(sysRoleQueryWrapper);
		for (SysRole role : sysRoles) {
			System.out.println(role);
		}
	}

	/**
	 * 条件删除
	 */
	@Test
	public void testDelete() {
		//创建条件构造器
		QueryWrapper<SysRole> sysRoleQueryWrapper = new QueryWrapper<>();
		sysRoleQueryWrapper.like("role_name", "管理员");
		int delete = sysRoleMapper.delete(sysRoleQueryWrapper);
		System.out.println(delete);
	}
}
