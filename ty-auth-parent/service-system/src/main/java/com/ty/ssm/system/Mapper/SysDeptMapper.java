package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.model.system.SysDept;
import com.ty.ssm.model.vo.SysDeptVo;
import org.apache.ibatis.annotations.Param;


public interface SysDeptMapper extends BaseMapper<SysDept> {
    //条件分页查询
     IPage<SysDept> selectDeptPage(Page<SysDept> sysDeptPage, @Param("vo") SysDeptVo sysDeptVo);
    //通过id删除
    Boolean deleteDeptById(String id);

    Boolean addDept(SysDept addDepts);
}
