package com.ty.ssm.system.controller;

import com.ty.ssm.common.result.Result;
import com.ty.ssm.common.utils.JwtHelper;
import com.ty.ssm.common.utils.MD5;
import com.ty.ssm.model.system.SysUser;
import com.ty.ssm.model.vo.LoginVo;
import com.ty.ssm.system.exception.CustomException;
import com.ty.ssm.system.service.ISysUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 @author ty
 @create 2023-03-27-23:32  
 */

@Api(tags = "用户登录接口")
@RestController
@RequestMapping("/admin/system/index")
public class IndexController {
	@Autowired
	private ISysUserService userService;

	/**
	 * 登录
	 * @return
	 */
	@PostMapping("/login")
	public Result login(@RequestBody LoginVo loginVo) {

		//根据用户名称查询数据库
		SysUser sysUser = userService.getUserInfoByUserName(loginVo.getUsername());
		if (sysUser == null) {
			throw new CustomException(20001, "用户不存在");
		}
		//判断密码是否一致
		String password = loginVo.getPassword();
		String encrypt = MD5.encrypt(password);
		if (!encrypt.equals(sysUser.getPassword())) {
			throw new CustomException(20002, "密码错误");
		}
		//判断当前用户是否可用
		if (sysUser.getStatus().intValue() == 0) {
			throw new CustomException(20003, "用户已经被禁用");
		}
		//根据用户id，用户名称生成token
		String token = JwtHelper.createToken(sysUser.getId(), sysUser.getUsername());
		//返回
		Map<String, Object> map = new HashMap<>();
		map.put("token", token);
		return Result.ok(map);
	}

	/**
	 * 获取用户信息
	 * @return
	 */
	@GetMapping("/info")
	public Result info(HttpServletRequest request) {
		//获取请求头中token
		String token = request.getHeader("token");
		//从token获取用户id，名称
		String userId = JwtHelper.getUserId(token);
		String username = JwtHelper.getUsername(token);
		//根据用户id获取用户信息
		Map<String, Object> sysMap = userService.getUserInfoMap(userId, username);
		return Result.ok(sysMap);
	}

	/**
	 * 退出
	 * @return
	 */
	@PostMapping("/logout")
	public Result logout() {
		return Result.ok();
	}

}

