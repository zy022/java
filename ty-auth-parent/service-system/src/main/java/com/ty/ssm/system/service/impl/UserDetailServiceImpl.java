package com.ty.ssm.system.service.impl;

import com.ty.ssm.model.system.SysUser;
import com.ty.ssm.model.vo.RouterVo;
import com.ty.ssm.system.custom.CustomUser;
import com.ty.ssm.system.service.ISysMenuService;
import com.ty.ssm.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 @author ty
 @create 2023-06-08-23:08  
 */

@Component
public class UserDetailServiceImpl implements UserDetailsService {
	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private ISysMenuService sysMenuService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		SysUser sysUser = sysUserService.getUserInfoByUserName(username);
		if (null == sysUser) {
			throw new UsernameNotFoundException("用户名不存在！");
		}

		if (sysUser.getStatus().intValue() == 0) {
			throw new RuntimeException("账号已停用");
		}
		//根据userId查询操作权限数据
		List<RouterVo> userMenuList = sysMenuService.getUserMenuList(sysUser.getId());
		//查询用户按钮权限数据
		List<String> userButtonList = sysMenuService.getUserButtonList(sysUser.getId());

		//转换成security要求格式数据
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		for (String s : userButtonList) {
			authorities.add(new SimpleGrantedAuthority(s.trim()));
		}

		return new CustomUser(sysUser, authorities);
	}
}
