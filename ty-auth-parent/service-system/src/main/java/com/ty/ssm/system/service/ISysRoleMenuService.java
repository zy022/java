package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ty.ssm.model.system.SysMenu;
import com.ty.ssm.model.system.SysRoleMenu;
import com.ty.ssm.model.vo.AssginMenuVo;

import java.util.List;

/**
 * <p>
 * 角色菜单 服务类
 * </p>
 *
 * @author ty
 * @since 2023-05-29
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

	List<SysMenu> getRoleAssignAuthorityById(String roleId);

	void assignAuthorityToRole(AssginMenuVo assginMenuVo);
}
