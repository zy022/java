package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ty.ssm.model.system.SysDept;
import com.ty.ssm.model.vo.SysDeptVo;

public interface ISysDeptService extends IService<SysDept> {
    //条件方法查询
    IPage<SysDept> selectDeptPage(Page<SysDept> sysDeptPage, SysDeptVo SysDeptVo);

    //通过id删除
    Boolean deleteDeptById(String id);

    Boolean addDept(SysDept addDepts);


//    boolean assignRolesToUser(AssginRoleVo assginRoleVo);
}

