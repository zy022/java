package com.ty.ssm.system.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.common.result.Result;
import com.ty.ssm.common.utils.MD5;
import com.ty.ssm.model.system.SysUser;
import com.ty.ssm.model.vo.SysUserQueryVo;
import com.ty.ssm.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author ty
 * @since 2023-04-17
 */
@Api(tags = "用户管理")
@RestController
@RequestMapping("/admin/system/sysUser")
public class SysUserController {

	@Autowired
	private ISysUserService sysUserService;

	@ApiOperation("用户列表")
	@GetMapping("/{page}/{limit}")
	public Result userList(@PathVariable Long page, @PathVariable Long limit, SysUserQueryVo sysUserQueryVo) {
		//创建page对象
		Page<SysUser> pageParam = new Page<SysUser>(page, limit);
		//调用service方法
		IPage<SysUser> pageUser = sysUserService.selectUserPage(pageParam, sysUserQueryVo);
		return Result.ok(pageUser);
	}

	@ApiOperation("添加用户")
	@PostMapping("/addUser")
	public Result addUser(@RequestBody SysUser user) {
		//将密码进行md5加密
		String password = user.getPassword();
		String encrypt = MD5.encrypt(password);
		user.setPassword(encrypt);
		boolean isSave = sysUserService.save(user);
		if (isSave) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	@ApiOperation("根据用户id查询用户")
	@GetMapping("/getUser/{id}")
	public Result getUserById(@PathVariable String id) {
		SysUser user = sysUserService.getById(id);
		return Result.ok(user);
	}

	/**
	 * 修改角色
	 *@RequestBody传递一个json数据，将json数据封装到对象，不能使用get提交
	 * @param sysUser
	 * @return
	 */
	@ApiOperation("修改用户信息")
	@PostMapping("/update")
	public Result updateUser(@RequestBody SysUser sysUser) {
		boolean isSuccess = sysUserService.updateById(sysUser);
		if (isSuccess) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	@ApiOperation("删除用户")
	@DeleteMapping("/removeUser/{id}")
	public Result removeUserById(@PathVariable String id) {
		boolean isSuccess = sysUserService.removeById(id);
		if (isSuccess) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	@ApiOperation("批量删除用户")
	@DeleteMapping("/batchRemoveUser")
	public Result batchRemoveUser(@RequestBody List<Long> ids) {
		boolean isSuccess = sysUserService.removeByIds(ids);
		if (isSuccess) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	@ApiOperation("改变用户状态")
	@GetMapping("/userStatusChange/{id}/{status}")
	public Result userStatusChange(@PathVariable String id, @PathVariable Integer status) {
		boolean isSuccess = sysUserService.userStatusChange(id, status);
		if (isSuccess) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}
}
