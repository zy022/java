package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.model.system.SysOperLog;
import com.ty.ssm.model.vo.SysOperLogQueryVo;
import com.ty.ssm.system.Mapper.SysOperLogMapper;
import com.ty.ssm.system.service.ISysOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 @author ty
 @create 2023-06-26-22:09  
 */

@Service
public class SysOperLogServiceImpl implements ISysOperLogService {
	@Autowired
	SysOperLogMapper sysOperLogMapper;

	/**
	 * 保存操作日志信息
	 *
	 * @param sysLog
	 */
	@Override
	public void saveSysLog(SysOperLog sysLog) {
		sysOperLogMapper.insert(sysLog);
	}

	@Override
	public IPage<SysOperLog> operlogPageInfo(Long page, Long limit, SysOperLogQueryVo sysOperLogQueryVo) {
		//构建page对象
		Page pageParam = new Page(page, limit);

		//获取查询条件
		String title = sysOperLogQueryVo.getTitle();
		String operName = sysOperLogQueryVo.getOperName();
		String createTimeBegin = sysOperLogQueryVo.getCreateTimeBegin();
		String createTimeEnd = sysOperLogQueryVo.getCreateTimeEnd();
		//封装条件
		QueryWrapper<SysOperLog> queryWrapper = new QueryWrapper<>();
		if (!StringUtils.isEmpty(title)) {
			queryWrapper.like("title", title);
		}
		if (!StringUtils.isEmpty(operName)) {
			queryWrapper.like("oper_name", operName);
		}
		if (!StringUtils.isEmpty(createTimeBegin)) {
			queryWrapper.ge("create_time", createTimeBegin);
		}
		if (!StringUtils.isEmpty(createTimeEnd)) {
			queryWrapper.le("create_time", createTimeEnd);
		}
		//调用mapper方法实现
		IPage<SysOperLog> pageModel = sysOperLogMapper.selectPage(pageParam, queryWrapper);
		return pageModel;
	}

	@Override
	public SysOperLog getById(Long id) {
		return sysOperLogMapper.selectById(id);
	}
}
