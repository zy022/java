package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ty.ssm.model.system.SysRole;
import com.ty.ssm.model.vo.AssginRoleVo;
import com.ty.ssm.model.vo.SysRoleQueryVo;

import java.util.Map;

/**
 @author ty
 @create 2023-03-07-23:06  
 */

public interface SysRoleService extends IService<SysRole> {
	//条件方法查询
	IPage<SysRole> selectPage(Page<SysRole> sysRolePage, SysRoleQueryVo sysRoleQueryVo);

	Map<String, Object> getRolesByUserId(String userId);

	boolean assignRolesToUser(AssginRoleVo assginRoleVo);
}
