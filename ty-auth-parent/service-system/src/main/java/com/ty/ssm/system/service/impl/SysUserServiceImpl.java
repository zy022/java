package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ty.ssm.model.system.SysUser;
import com.ty.ssm.model.vo.RouterVo;
import com.ty.ssm.model.vo.SysUserQueryVo;
import com.ty.ssm.system.Mapper.SysUserMapper;
import com.ty.ssm.system.service.ISysMenuService;
import com.ty.ssm.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ty
 * @since 2023-04-17
 */
@Service
@Transactional
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

	@Autowired
	private ISysMenuService sysMenuService;
	//@Autowired
	//private ISysUserRoleService roleService;
	//@Autowired
	//private ISysRoleMenuService sysRoleMenuService;

	@Override
	public IPage<SysUser> selectUserPage(Page<SysUser> pageParam, SysUserQueryVo sysUserQueryVo) {
		return baseMapper.selectUserPage(pageParam, sysUserQueryVo);
	}

	@Override
	public boolean userStatusChange(String id, Integer status) {
		SysUser sysUser = baseMapper.selectById(id);
		sysUser.setStatus(status);
		int i = baseMapper.updateById(sysUser);
		return i > 0;
	}

	/**
	 * 根据用户名称查询用户信息
	 * @param username
	 * @return
	 */
	@Override
	public SysUser getUserInfoByUserName(String username) {
		QueryWrapper<SysUser> query = new QueryWrapper<>();
		query.eq("username", username);
		SysUser sysUser = baseMapper.selectOne(query);
		return sysUser;
	}

	/**
	 * 获取用户信息(基本信息，菜单权限，按钮权限)
	 * @param userId
	 * @param username
	 * @return
	 */
	@Override
	public Map<String, Object> getUserInfoMap(String userId, String username) {
		SysUser sysUser = baseMapper.selectById(userId);
		HashMap<String, Object> userInfoMap = new HashMap<>();
		userInfoMap.put("userId", userId);
		userInfoMap.put("name", username);
		userInfoMap.put("avatar", "https://oss.aliyuncs.com/aliyun_id_photo_bucket/default_handsome.jpg");
		userInfoMap.put("roles", "[\"admin\"]");

		////根据用户id获取角色权限
		//QueryWrapper<SysUserRole> sysUserRoleQueryWrapper = new QueryWrapper<>();
		//sysUserRoleQueryWrapper.eq("user_id", userId);
		//SysUserRole userRole = roleService.getOne(sysUserRoleQueryWrapper);
		////根据角色权限查询菜单权限
		//List<SysMenu> roleAssignAuthority = sysRoleMenuService.getRoleAssignAuthorityById(userRole.getRoleId());

		//根据用户id查询菜单权限值
		List<RouterVo> routerVoList = sysMenuService.getUserMenuList(userId);
		//根据用户id查询按钮权限
		List<String> buttonsList = sysMenuService.getUserButtonList(userId);

		//菜单权限
		userInfoMap.put("routers", routerVoList);
		//按钮权限
		userInfoMap.put("buttons", buttonsList);
		return userInfoMap;
	}
}
