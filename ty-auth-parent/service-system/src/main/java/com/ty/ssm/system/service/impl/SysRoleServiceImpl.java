package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ty.ssm.model.system.SysRole;
import com.ty.ssm.model.system.SysUserRole;
import com.ty.ssm.model.vo.AssginRoleVo;
import com.ty.ssm.model.vo.SysRoleQueryVo;
import com.ty.ssm.system.Mapper.SysRoleMapper;
import com.ty.ssm.system.Mapper.SysUserRoleMapper;
import com.ty.ssm.system.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 @author ty
 @create 2023-03-07-23:07  
 */

@Service
@Transactional
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	@Override
	public IPage<SysRole> selectPage(Page<SysRole> sysRolePage, SysRoleQueryVo sysRoleQueryVo) {
		IPage<SysRole> pageModel = baseMapper.selectPage(sysRolePage, sysRoleQueryVo);
		return pageModel;
	}

	/**
	 * 获取用户的角色信息
	 * @param userId
	 * @return
	 */
	@Override
	public Map<String, Object> getRolesByUserId(String userId) {
		//获取所有角色
		List<SysRole> sysRoles = baseMapper.selectList(null);
		//根据用户id查询用户已经分配的角色信息
		QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
		wrapper.eq("user_id", userId);
		List<SysUserRole> sysUserRoles = sysUserRoleMapper.selectList(wrapper);
		//获取所有角色id
		List<String> userRoleIds = new ArrayList<>();
		for (SysUserRole sysUserRole : sysUserRoles) {
			String roleId = sysUserRole.getRoleId();
			userRoleIds.add(roleId);
		}
		//封装到map集合
		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("allRoles", sysRoles);//所有角色
		returnMap.put("userRoleIds", userRoleIds);//用户已经分配的角色id

		return returnMap;
	}

	/**
	 * 根据用户分配角色
	 * @param assginRoleVo
	 * @return
	 */
	@Override
	public boolean assignRolesToUser(AssginRoleVo assginRoleVo) {
		//根据用户id删除之前分配的角色
		QueryWrapper<SysUserRole> wrapper = new QueryWrapper<>();
		wrapper.eq("user_id", assginRoleVo.getUserId());
		sysUserRoleMapper.delete(wrapper);

		//获取所有角色id，添加到对应角色用户关系表
		//角色id列表
		List<String> roleIdList = assginRoleVo.getRoleIdList();
		for (String id : roleIdList) {
			SysUserRole sysuserRole = new SysUserRole();
			sysuserRole.setUserId(assginRoleVo.getUserId());
			sysuserRole.setRoleId(id);
			sysUserRoleMapper.insert(sysuserRole);
		}
		return true;
	}
}
