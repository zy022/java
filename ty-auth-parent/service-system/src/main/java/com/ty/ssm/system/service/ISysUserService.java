package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ty.ssm.model.system.SysUser;
import com.ty.ssm.model.vo.SysUserQueryVo;

import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ty
 * @since 2023-04-17
 */
public interface ISysUserService extends IService<SysUser> {

	//用户列表方法
	IPage<SysUser> selectUserPage(Page<SysUser> pageParam, SysUserQueryVo sysUserQueryVo);

	//更改用户状态
	boolean userStatusChange(String id, Integer status);

	SysUser getUserInfoByUserName(String username);

	Map<String, Object> getUserInfoMap(String userId, String userName);
}
