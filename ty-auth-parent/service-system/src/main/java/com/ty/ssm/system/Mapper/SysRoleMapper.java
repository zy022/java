package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.model.system.SysRole;
import com.ty.ssm.model.vo.SysRoleQueryVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 @author ty
 @create 2023-03-06-22:49  
 */

@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {
	//条件分页查询
	public IPage<SysRole> selectPage(Page<SysRole> sysRolePage, @Param("vo") SysRoleQueryVo sysRoleQueryVo);
}
