package com.ty.ssm.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ty.ssm.common.result.Result;
import com.ty.ssm.model.system.SysLoginLog;
import com.ty.ssm.model.vo.SysLoginLogQueryVo;
import com.ty.ssm.system.service.ISysLoginLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录日志管理
 *
 @author ty
 @create 2023-06-26-22:20  
 */

@Api(value = "SysLoginLog管理", tags = "SysLoginLog管理")
@RestController
@RequestMapping(value = "/admin/system/sysLoginLog")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SysLoginLogController {
	@Autowired
	private ISysLoginLogService sysLoginLogService;

	@ApiOperation(value = "获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result loginlogPageInfo(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			@ApiParam(name = "sysLoginLogVo", value = "查询对象", required = false)
					SysLoginLogQueryVo sysLoginLogQueryVo) {
		IPage<SysLoginLog> pageModel = sysLoginLogService.loginlogPageInfo(page, limit, sysLoginLogQueryVo);
		return Result.ok(pageModel);
	}

	@ApiOperation(value = "获取")
	@GetMapping("get/{id}")
	public Result getById(@PathVariable String id) {
		SysLoginLog sysLoginLog = sysLoginLogService.getById(id);
		return Result.ok(sysLoginLog);
	}
}
