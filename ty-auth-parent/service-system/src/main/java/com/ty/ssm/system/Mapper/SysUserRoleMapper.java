package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ty.ssm.model.system.SysUserRole;

/**
 * <p>
 * 用户角色 Mapper 接口
 * </p>
 *
 * @author ty
 * @since 2023-05-08
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
