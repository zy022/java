package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ty.ssm.model.system.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author ty
 * @since 2023-05-15
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

	/**
	 * 根据用户ID查询菜单权限
	 * @param userId
	 * @return
	 */
	List<SysMenu> getUserMenuList(@Param("userId") String userId);
}
