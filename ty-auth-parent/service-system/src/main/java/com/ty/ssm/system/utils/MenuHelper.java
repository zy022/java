package com.ty.ssm.system.utils;

import com.ty.ssm.model.system.SysMenu;

import java.util.ArrayList;
import java.util.List;

/**
 @author ty
 @create 2023-05-18-22:46  
 */

public class MenuHelper {
	/**
	 * 构建菜单树形结构
	 * @param sysMenus
	 * @return
	 */
	public static List<SysMenu> buildTree(List<SysMenu> sysMenus) {
		//创建集合封装数据
		List<SysMenu> tree = new ArrayList<SysMenu>();
		//遍历所有菜单集合
		for (SysMenu sysMenu : sysMenus) {
			//顶级菜单
			if (Long.parseLong(sysMenu.getParentId()) == 0) {
				tree.add(findChild(sysMenu, sysMenus));
			}
		}
		return tree;
	}

	/**
	 * 从根节点进行递归查询，查询子节点
	 * 判断Id == parentId，如果相同是子节点，进行数据封装
	 * @param sysMenu
	 * @param sysMenus
	 * @return
	 */
	private static SysMenu findChild(SysMenu sysMenu, List<SysMenu> sysMenus) {
		sysMenu.setChildren(new ArrayList<SysMenu>());
		String id = sysMenu.getId();
		//遍历-
		for (SysMenu menu : sysMenus) {
			String parentId = menu.getParentId();
			if (0 == parentId.compareTo(id)) {
				sysMenu.getChildren().add(findChild(menu, sysMenus));
			}
		}
		return sysMenu;
	}
}
