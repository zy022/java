package com.ty.ssm.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.common.result.Result;
import com.ty.ssm.model.system.SysDept;
import com.ty.ssm.model.vo.SysDeptVo;
import com.ty.ssm.system.service.ISysDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "部门管理")
@RestController
@RequestMapping("/admin/system/sysDept")
public class SysDeptController {
    @Autowired
    private ISysDeptService sysDeptService;

    @ApiOperation("用户列表")
    @GetMapping("/{page}/{limit}")
    public Result userList(@PathVariable Long page, @PathVariable Long limit, SysDeptVo sysDeptVo) {
        //创建page对象
        Page<SysDept> pageParam = new Page<SysDept>(page, limit);
        //调用service方法
        IPage<SysDept> pageUser = sysDeptService.selectDeptPage(pageParam, sysDeptVo);
        return Result.ok(pageUser);
    }

@GetMapping("/deleteDept")
    public Result deleteDeptById(String id){
        Boolean boolResult = sysDeptService.deleteDeptById(id);
        if(boolResult){
            return Result.ok();
        }else{
            return Result.fail();
        }
    }

    @PostMapping("/addDept")
    public Result addDept(@RequestBody SysDept addDepts){
        Boolean boolResult = sysDeptService.addDept(addDepts);
        if(boolResult){
            return Result.ok();
        }else{
            return Result.fail();
        }
    }

}
