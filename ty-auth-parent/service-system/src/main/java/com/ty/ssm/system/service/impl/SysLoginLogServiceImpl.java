package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.model.system.SysLoginLog;
import com.ty.ssm.model.vo.SysLoginLogQueryVo;
import com.ty.ssm.system.Mapper.SysLoginLogMapper;
import com.ty.ssm.system.service.ISysLoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * 登录日志实现类
 *
 @author ty
 @create 2023-06-26-21:35  
 */

@Service
public class SysLoginLogServiceImpl implements ISysLoginLogService {

	@Autowired
	SysLoginLogMapper sysLoginLogMapper;

	/**
	 * 记录日志信息
	 * @param username
	 * @param status
	 * @param ip
	 * @param msg
	 */
	@Override
	public void recordLoginLog(String username, Integer status, String ip, String msg) {
		SysLoginLog sysLoginLog = new SysLoginLog();
		sysLoginLog.setUserName(username);
		sysLoginLog.setStatus(status);
		sysLoginLog.setIpAddress(ip);
		sysLoginLog.setMsg(msg);

		sysLoginLogMapper.insert(sysLoginLog);
	}

	@Override
	public IPage<SysLoginLog> loginlogPageInfo(Long page, Long limit, SysLoginLogQueryVo sysLoginLogQueryVo) {
		//构建page对象
		Page pageParam = new Page(page, limit);

		//获取查询条件
		String username = sysLoginLogQueryVo.getUsername();
		String createTimeBegin = sysLoginLogQueryVo.getCreateTimeBegin();
		String createTimeEnd = sysLoginLogQueryVo.getCreateTimeEnd();
		//封装条件
		QueryWrapper<SysLoginLog> queryWrapper = new QueryWrapper<>();
		if (!StringUtils.isEmpty(username)) {
			queryWrapper.like("username", username);
		}
		if (!StringUtils.isEmpty(createTimeBegin)) {
			queryWrapper.ge("create_time", createTimeBegin);
		}
		if (!StringUtils.isEmpty(createTimeEnd)) {
			queryWrapper.le("create_time", createTimeEnd);
		}
		//调用mapper方法实现
		IPage<SysLoginLog> pageModel = sysLoginLogMapper.selectPage(pageParam, queryWrapper);
		return pageModel;
	}

	@Override
	public SysLoginLog getById(String id) {
		return sysLoginLogMapper.selectById(id);
	}
}
