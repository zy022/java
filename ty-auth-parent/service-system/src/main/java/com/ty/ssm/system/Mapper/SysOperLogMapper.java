package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ty.ssm.model.system.SysOperLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 操作日志
 @author ty
 @create 2023-06-26-22:12  
 */
@Repository
@Mapper
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {
}
