package com.ty.ssm.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ty.ssm.common.result.Result;
import com.ty.ssm.model.system.SysRole;
import com.ty.ssm.model.vo.AssginRoleVo;
import com.ty.ssm.model.vo.SysRoleQueryVo;
import com.ty.ssm.system.annotation.Log;
import com.ty.ssm.system.enums.BusinessType;
import com.ty.ssm.system.exception.CustomException;
import com.ty.ssm.system.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 @author ty
 @create 2023-03-13-22:41  
 */

@Api(tags = "角色管理")
@RestController
@RequestMapping("/admin/system/sysRole/")
public class SysRoleController {
	@Autowired
	private SysRoleService sysRoleService;

	//查询记录
	@GetMapping("findAll")
	@ApiOperation(value = "获取全部角色列表")
	public Result findAllSysRoles() {
		try {
			int a = 9 / 0;
		} catch (Exception e) {
			throw new CustomException(20001, "执行自定义异常处理");
		}

		List<SysRole> list = sysRoleService.list();
		return Result.ok(list);
	}

	@PreAuthorize("hasAuthority('bnt.sysRole.remove')")
	//逻辑删除
	@DeleteMapping("remove/{id}")
	@ApiOperation(value = "逻辑删除接口")
	public Result removeSysRole(@PathVariable Long id) {
		boolean b = sysRoleService.removeById(id);
		if (b) {
			return Result.ok();
		}
		return Result.fail();
	}

	/**
	 * 条件分页查询
	 * page 当前页
	 * limit 每页的记录数
	 * @return
	 */
	@PreAuthorize("hasAuthority('bnt.sysRole.list')")//通过@PreAuthorize标签控制controller层接口权限
	@ApiOperation("条件分页查询")
	@GetMapping("{page}/{limit}")
	public Result findQueryRolePage(
			@PathVariable Long page,
			@PathVariable Long limit,
			SysRoleQueryVo sysRoleQueryVo) {

		//构建page对象
		Page<SysRole> sysRolePage = new Page<>(page, limit);
		//调用service方法
		IPage<SysRole> pageModel = sysRoleService.selectPage(sysRolePage, sysRoleQueryVo);
		//返回
		return Result.ok(pageModel);
	}

	/**
	 * 添加角色
	 *@RequestBody传递一个json数据，将json数据封装到对象，不能使用get提交
	 * @param sysRole
	 * @return
	 */
	@Log(title = "角色管理", businessType = BusinessType.INSERT)
	@PreAuthorize("hasAuthority('bnt.sysRole.add')")
	@ApiOperation("添加角色")
	@PostMapping("save")
	public Result AddRole(@RequestBody SysRole sysRole) {
		boolean isSave = sysRoleService.save(sysRole);
		if (isSave) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	/**
	 * 根据id查找
	 * @param id
	 * @return
	 */
	@PreAuthorize("hasAuthority('bnt.sysRole.list')")
	@ApiOperation("根据id查找")
	@GetMapping("findRoleById/{id}")
	public Result findRoleById(@PathVariable String id) {
		SysRole sysRole = sysRoleService.getById(id);
		return Result.ok(sysRole);
	}

	/**
	 * 修改角色
	 *@RequestBody传递一个json数据，将json数据封装到对象，不能使用get提交
	 * @param sysRole
	 * @return
	 */
	@PreAuthorize("hasAuthority('bnt.sysRole.update')")
	@ApiOperation("修改角色")
	@PostMapping("update")
	public Result updateRole(@RequestBody SysRole sysRole) {
		boolean isSuccess = sysRoleService.updateById(sysRole);
		if (isSuccess) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	/**
	 * 批量删除
	 * 多个id值
	 * json数组格式对应java中list集合
	 * @RequestBody传递一个json数据，将json数据封装到对象，不能使用get提交
	 * @param ids
	 * @return
	 */
	@ApiOperation("批量删除角色")
	@DeleteMapping("batchRemove")
	public Result batchRemove(@RequestBody List<Long> ids) {
		boolean isSuccess = sysRoleService.removeByIds(ids);
		if (isSuccess) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	@ApiOperation(value = "根据用户获取角色数据")
	@GetMapping("GetRolesByUserId/{userId}")
	public Result getRolesByUserId(@PathVariable String userId) {
		Map<String, Object> roleMap = sysRoleService.getRolesByUserId(userId);
		return Result.ok(roleMap);
	}

	@ApiOperation(value = "根据用户分配角色")
	@PostMapping("AssignRolesToUser")
	public Result assignRolesToUser(@RequestBody AssginRoleVo assginRoleVo) {
		boolean b = sysRoleService.assignRolesToUser(assginRoleVo);
		return Result.ok();
	}
}
