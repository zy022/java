package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ty.ssm.model.system.SysMenu;
import com.ty.ssm.model.vo.RouterVo;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author ty
 * @since 2023-05-15
 */
public interface ISysMenuService extends IService<SysMenu> {

	List<SysMenu> getAllMenu();

	void removeMenuById(String id);

	List<RouterVo> getUserMenuList(String userId);

	List<String> getUserButtonList(String userId);
}
