package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ty.ssm.model.system.SysDept;
import com.ty.ssm.model.vo.SysDeptVo;
import com.ty.ssm.system.Mapper.SysDeptMapper;
import com.ty.ssm.system.service.ISysDeptService;
import com.ty.ssm.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {
    @Autowired
    private ISysMenuService sysMenuService;
    @Override
    public IPage<SysDept> selectDeptPage(Page<SysDept> pageParam, SysDeptVo sysDeptVo) {
        return baseMapper.selectDeptPage(pageParam, sysDeptVo);
    }

    @Override
    public Boolean deleteDeptById(String id) {
        return baseMapper.deleteDeptById(id);
    }

    @Override
    public Boolean addDept(SysDept addDepts) {
        return baseMapper.addDept(addDepts);
    }


}


