package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ty.ssm.model.system.SysMenu;
import com.ty.ssm.model.vo.RouterVo;
import com.ty.ssm.system.Mapper.SysMenuMapper;
import com.ty.ssm.system.exception.CustomException;
import com.ty.ssm.system.service.ISysMenuService;
import com.ty.ssm.system.utils.MenuHelper;
import com.ty.ssm.system.utils.RouteHelp;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author ty
 * @since 2023-05-15
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

	/**
	 * 菜单列表
	 * @return
	 */
	@Override
	public List<SysMenu> getAllMenu() {
		//获取所有菜单
		List<SysMenu> sysMenus = baseMapper.selectList(null);
		//菜单转换为要求的数据格式
		List<SysMenu> sysMenuTreeList = MenuHelper.buildTree(sysMenus);
		return sysMenuTreeList;
	}

	/**
	 * 根据id删除菜单
	 * @param id
	 * @return
	 */
	@Override
	public void removeMenuById(String id) {
		//查询当前菜单下是否存在子菜单
		QueryWrapper<SysMenu> query = new QueryWrapper<>();
		query.eq("parent_id", id);
		Integer count = baseMapper.selectCount(query);
		if (count > 0) {
			throw new CustomException(201, "当前菜单存在子菜单");
		}
		//删除
		baseMapper.deleteById(id);
	}

	/**
	 * 根据用户id查询菜单权限
	 * @param userId
	 * @return
	 */
	@Override
	public List<RouterVo> getUserMenuList(String userId) {
		List<SysMenu> sysMenuList = null;
		//admin是超级管理员，拥有所有权限
		if (userId.equals("1")) {
			QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
			wrapper.eq("status", 1);
			wrapper.orderByAsc("sort_value");
			sysMenuList = baseMapper.selectList(wrapper);
		} else {
			//其他用户
			sysMenuList = baseMapper.getUserMenuList(userId);
		}

		//构建树形结构
		List<SysMenu> sysMenuTreeList = MenuHelper.buildTree(sysMenuList);
		//转换为前端要求的格式
		List<RouterVo> routerVoList = RouteHelp.buildRouters(sysMenuTreeList);

		return routerVoList;
	}

	/**
	 * 根据用户id查询按钮权限值
	 * @param userId
	 * @return
	 */
	@Override
	public List<String> getUserButtonList(String userId) {
		//当前用户是否是管理员
		List<SysMenu> sysMenuList = null;
		//admin是超级管理员，拥有所有权限
		if ("1".equals(userId)) {
			QueryWrapper<SysMenu> wrapper = new QueryWrapper<>();
			wrapper.eq("status", 1);
			wrapper.orderByAsc("sort_value");
			sysMenuList = baseMapper.selectList(wrapper);
		} else {
			//其他用户
			sysMenuList = baseMapper.getUserMenuList(userId);
		}
		List<String> buttonList = new ArrayList<>();
		for (SysMenu sysMenu : sysMenuList) {
			if (sysMenu.getType() == 2) {
				buttonList.add(sysMenu.getPerms());
			}
		}
		return buttonList;
	}

}
