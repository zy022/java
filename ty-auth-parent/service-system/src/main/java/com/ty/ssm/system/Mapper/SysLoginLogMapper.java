package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ty.ssm.model.system.SysLoginLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 登录日志
 *
 @author ty
 @create 2023-06-26-21:37  
 */
@Repository
@Mapper
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
}
