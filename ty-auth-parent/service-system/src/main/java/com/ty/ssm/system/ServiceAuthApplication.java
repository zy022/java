package com.ty.ssm.system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 @author ty
 @create 2023-03-02-22:31  
 */

@SpringBootApplication
@MapperScan(basePackages = "com.ty.ssm.system.Mapper")
public class ServiceAuthApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServiceAuthApplication.class, args);
	}
}
