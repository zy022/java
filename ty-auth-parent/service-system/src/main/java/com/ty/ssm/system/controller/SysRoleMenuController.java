package com.ty.ssm.system.controller;


import com.ty.ssm.common.result.Result;
import com.ty.ssm.model.system.SysMenu;
import com.ty.ssm.model.vo.AssginMenuVo;
import com.ty.ssm.system.service.ISysRoleMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 角色菜单 前端控制器
 * </p>
 *
 * @author ty
 * @since 2023-05-29
 */
@Api(tags = "角色分配权限")
@RestController
@RequestMapping("admin/system/sysRoleMenu")
public class SysRoleMenuController {

	@Autowired
	private ISysRoleMenuService service;

	@ApiOperation(value = "根据角色获取菜单")
	@GetMapping("/GetRoleAssignAuthorityById/{roleId}")
	public Result getRoleAssignAuthorityById(@PathVariable String roleId) {
		List<SysMenu> list = service.getRoleAssignAuthorityById(roleId);
		return Result.ok(list);
	}

	@ApiOperation(value = "给角色分配权限")
	@PostMapping("/AssignAuthorityToRole")
	public Result assignAuthorityToRole(@RequestBody AssginMenuVo assginMenuVo) {
		service.assignAuthorityToRole(assginMenuVo);
		return Result.ok();
	}
}
