package com.ty.ssm.system.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ty.ssm.model.system.SysRoleMenu;

/**
 * <p>
 * 角色菜单 Mapper 接口
 * </p>
 *
 * @author ty
 * @since 2023-05-29
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
