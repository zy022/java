package com.ty.ssm.system.controller;


import com.ty.ssm.common.result.Result;
import com.ty.ssm.model.system.SysMenu;
import com.ty.ssm.system.service.ISysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author ty
 * @since 2023-05-15
 */
@Api(tags = "菜单管理")
@RestController
@RequestMapping("/admin/system/sysMenu/")
public class SysMenuController {
	@Autowired
	private ISysMenuService service;

	/**
	 * 菜单的列表方法（树形）
	 * @return
	 */
	@ApiOperation("菜单的列表方法")
	@GetMapping("GetAllMenu")
	public Result getAllMenu() {
		List<SysMenu> menus = service.getAllMenu();
		return Result.ok(menus);
	}

	/**
	 * 添加菜单
	 * @param sysMenu
	 * @return
	 */
	@ApiOperation("添加菜单")
	@PostMapping("AddMenu")
	public Result addMenu(@RequestBody SysMenu sysMenu) {
		boolean save = service.save(sysMenu);
		if (save) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	/**
	 * 根据id查询菜单
	 * @param id
	 * @return
	 */
	@ApiOperation("根据id查询菜单")
	@GetMapping("GetMenuById/{id}")
	public Result getMenuById(@PathVariable String id) {
		SysMenu menu = service.getById(id);
		return Result.ok(menu);
	}

	/**
	 * 修改菜单
	 * @return
	 */
	@ApiOperation("修改菜单")
	@PostMapping("UpdateMenu")
	public Result updateMenu(@RequestBody SysMenu sysMenu) {
		boolean b = service.updateById(sysMenu);
		if (b) {
			return Result.ok();
		} else {
			return Result.fail();
		}
	}

	/**
	 * 删除菜单
	 * @return
	 */
	@ApiOperation("删除菜单")
	@DeleteMapping("RemoveMenu/{id}")
	public Result removeMenu(@PathVariable String id) {
		service.removeMenuById(id);
		return Result.ok();
	}

}
