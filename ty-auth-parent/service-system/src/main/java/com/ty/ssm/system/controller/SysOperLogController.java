package com.ty.ssm.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ty.ssm.common.result.Result;
import com.ty.ssm.model.system.SysOperLog;
import com.ty.ssm.model.vo.SysOperLogQueryVo;
import com.ty.ssm.system.service.ISysOperLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 @author ty
 @create 2023-06-26-22:41  
 */

@Api(value = "SysOperLog管理", tags = "SysOperLog管理")
@RestController
@RequestMapping(value = "/admin/system/sysOperLog")
@SuppressWarnings({"unchecked", "rawtypes"})
public class SysOperLogController {
	@Autowired
	private ISysOperLogService sysOperLogService;

	@ApiOperation(value = "获取分页列表")
	@GetMapping("{page}/{limit}")
	public Result operlogPageInfo(
			@ApiParam(name = "page", value = "当前页码", required = true)
			@PathVariable Long page,
			@ApiParam(name = "limit", value = "每页记录数", required = true)
			@PathVariable Long limit,
			@ApiParam(name = "sysOperLogVo", value = "查询对象", required = false)
					SysOperLogQueryVo sysOperLogQueryVo) {
		IPage<SysOperLog> pageModel = sysOperLogService.operlogPageInfo(page, limit, sysOperLogQueryVo);
		return Result.ok(pageModel);
	}

	@ApiOperation(value = "获取")
	@GetMapping("get/{id}")
	public Result getById(@PathVariable Long id) {
		SysOperLog sysOperLog = sysOperLogService.getById(id);
		return Result.ok(sysOperLog);
	}

}
