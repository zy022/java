package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ty.ssm.model.system.SysUserRole;

/**
 * <p>
 * 用户角色 服务类
 * </p>
 *
 * @author ty
 * @since 2023-05-08
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
