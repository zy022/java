package com.ty.ssm.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 用户角色 前端控制器
 * </p>
 *
 * @author ty
 * @since 2023-05-08
 */
@Controller
@RequestMapping("/system/sys-user-role")
public class SysUserRoleController {

}
