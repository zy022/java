package com.ty.ssm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ty.ssm.model.system.SysMenu;
import com.ty.ssm.model.system.SysRoleMenu;
import com.ty.ssm.model.vo.AssginMenuVo;
import com.ty.ssm.system.Mapper.SysMenuMapper;
import com.ty.ssm.system.Mapper.SysRoleMenuMapper;
import com.ty.ssm.system.service.ISysRoleMenuService;
import com.ty.ssm.system.utils.MenuHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 角色菜单 服务实现类
 * </p>
 *
 * @author ty
 * @since 2023-05-29
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

	@Autowired
	private SysMenuMapper sysMenuMapper;

	/**
	 * 获取角色已经分配的菜单权限
	 * @param roleId
	 * @return
	 */
	@Override
	public List<SysMenu> getRoleAssignAuthorityById(String roleId) {
		//获取所有status为1的权限列表
		QueryWrapper<SysMenu> sysMenuQueryWrapper = new QueryWrapper<SysMenu>();
		sysMenuQueryWrapper.eq("status", 1);
		List<SysMenu> menuList = sysMenuMapper.selectList(sysMenuQueryWrapper);
		//根据角色id获取角色权限
		QueryWrapper<SysRoleMenu> sysRoleMenuQueryWrapper = new QueryWrapper<>();
		sysRoleMenuQueryWrapper.eq("role_id", roleId);
		List<SysRoleMenu> roleMenus = baseMapper.selectList(sysRoleMenuQueryWrapper);
		//获取该角色已分配的所有权限id
		List<String> roleMenuIds = new ArrayList<>();
		for (SysRoleMenu roleMenu : roleMenus) {
			roleMenuIds.add(roleMenu.getMenuId());
		}
		//遍历所有权限列表
		for (SysMenu sysMenu : menuList) {
			boolean contains = roleMenuIds.contains(sysMenu.getId());
			sysMenu.setSelect(contains);
		}
		//将权限列表转换为权限树
		List<SysMenu> sysMenus = MenuHelper.buildTree(menuList);
		return sysMenus;
	}

	/**
	 * 给角色分配菜单权限
	 * @param assginMenuVo
	 */
	@Override
	public void assignAuthorityToRole(AssginMenuVo assginMenuVo) {
		//删除已分配的权限
		baseMapper.deleteById(assginMenuVo.getRoleId());
		//遍历所有已选择的权限id
		for (String menuId : assginMenuVo.getMenuIdList()) {
			if (menuId != null) {
				//创建SysRoleMenu对象
				SysRoleMenu sysRoleMenu = new SysRoleMenu();
				sysRoleMenu.setMenuId(menuId);
				sysRoleMenu.setRoleId(assginMenuVo.getRoleId());
				//添加新权限
				baseMapper.insert(sysRoleMenu);
			}
		}
	}
}
