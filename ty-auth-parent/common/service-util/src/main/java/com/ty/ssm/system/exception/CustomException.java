package com.ty.ssm.system.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 @author ty
 @create 2023-03-22-23:29  
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomException extends RuntimeException {

	/**
	 * 状态码
	 */
	private Integer code;

	/**
	 * 错误信息
	 */
	private String message;


}
