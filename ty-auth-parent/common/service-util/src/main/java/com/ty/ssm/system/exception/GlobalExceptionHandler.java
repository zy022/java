package com.ty.ssm.system.exception;

import com.ty.ssm.common.result.Result;
import com.ty.ssm.common.result.ResultCodeEnum;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.file.AccessDeniedException;

/**
 @author ty
 @create 2023-03-21-23:28  
 */

@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 全局异常处理
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public Result error(Exception exception) {
		exception.printStackTrace();
		return Result.fail().message("执行全局异常处理");
	}

	/**
	 * 特定异常处理
	 * @param e
	 * @return
	 */
	@ExceptionHandler(ArithmeticException.class)
	@ResponseBody
	public Result error(ArithmeticException e) {
		e.printStackTrace();
		return Result.fail().message("执行了特定异常处理");
	}

	/**
	 * 自定义异常处理
	 * @param e
	 * @return
	 */
	@ExceptionHandler(CustomException.class)
	@ResponseBody
	public Result error(CustomException e) {
		e.printStackTrace();
		return Result.fail().code(e.getCode()).message(e.getMessage());
	}

	/**
	 * spring security异常
	 * @param e
	 * @return
	 */
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	public Result error(AccessDeniedException e) throws AccessDeniedException {
		return Result.build(null, ResultCodeEnum.PERMISSION);
	}

}
