package com.ty.ssm.system.custom;

import com.ty.ssm.common.utils.MD5;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * MD5密码加密
 @author ty
 @create 2023-06-08-23:00  
 */
@Configuration
public class CustomMD5Password implements PasswordEncoder {
	@Override
	public String encode(CharSequence rawPassword) {
		return MD5.encrypt(rawPassword.toString());
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encodedPassword.equals(MD5.encrypt(rawPassword.toString()));
	}
}
