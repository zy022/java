package com.ty.ssm.system.fillter;

import com.alibaba.fastjson.JSON;
import com.ty.ssm.common.result.Result;
import com.ty.ssm.common.result.ResultCodeEnum;
import com.ty.ssm.common.utils.JwtHelper;
import com.ty.ssm.common.utils.ResponseUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 认证解析token过滤器
 * 因为用户登录状态在token中存储在客户端，所以每次请求接口请求头携带token，
 * 后台通过自定义token过滤器拦截解析token完成认证并填充用户信息实体。
 @author ty
 @create 2023-06-19-22:48  
 */

public class TokenAuthenticationFilter extends OncePerRequestFilter {

	private RedisTemplate redisTemplate;

	public TokenAuthenticationFilter(RedisTemplate redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
	                                FilterChain filterChain) throws ServletException, IOException {
		logger.info("uri:" + httpServletRequest.getRequestURI());
		//如果是登录接口，直接放行
		if ("/admin/system/index/login".equals(httpServletRequest.getRequestURI())) {
			filterChain.doFilter(httpServletRequest, httpServletResponse);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = getAuthentication(httpServletRequest);
		if (null != authentication) {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			filterChain.doFilter(httpServletRequest, httpServletResponse);
		} else {
			ResponseUtil.out(httpServletResponse, Result.build(null, ResultCodeEnum.PERMISSION));
		}
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
		// token置于header里
		String token = request.getHeader("token");
		logger.info("token:" + token);
		if (!StringUtils.isEmpty(token)) {
			String useruame = JwtHelper.getUsername(token);
			logger.info("useruame:" + useruame);
			if (!StringUtils.isEmpty(useruame)) {
				String authoritiesString = (String) redisTemplate.opsForValue().get(useruame);
				List<Map> mapList = JSON.parseArray(authoritiesString, Map.class);
				List<SimpleGrantedAuthority> authorities = new ArrayList<>();
				for (Map map : mapList) {
					authorities.add(new SimpleGrantedAuthority((String) map.get("authority")));
				}
				return new UsernamePasswordAuthenticationToken(useruame, null, authorities);
				//return new UsernamePasswordAuthenticationToken(useruame, null, Collections.emptyList());
			}
		}
		return null;
	}
}
