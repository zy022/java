package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ty.ssm.model.system.SysLoginLog;
import com.ty.ssm.model.vo.SysLoginLogQueryVo;

/**
 *
 * 登录日志接口
 @author ty
 @create 2023-06-26-21:34  
 */

public interface ISysLoginLogService {
	/**
	 * 记录日志信息
	 * @param username
	 * @param status
	 * @param ip
	 * @param msg
	 */
	public void recordLoginLog(String username, Integer status, String ip, String msg);

	/**
	 *分页查询登录日志
	 * @param page
	 * @param limit
	 * @param sysLoginLogQueryVo
	 * @return
	 */
	public IPage<SysLoginLog> loginlogPageInfo(Long page, Long limit, SysLoginLogQueryVo sysLoginLogQueryVo);

	/**
	 * 根据日志id查询登录日志
	 * @param id
	 * @return
	 */
	public SysLoginLog getById(String id);
}
