package com.ty.ssm.system.enums;

/**
 * 操作类别枚举
 *
 @author ty
 @create 2023-06-26-21:56  
 */

public enum OperatorType {
	/**
	 * 其它
	 */
	OTHER,

	/**
	 * 后台用户
	 */
	MANAGE,

	/**
	 * 手机端用户
	 */
	MOBILE
}
