package com.ty.ssm.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ty.ssm.model.system.SysOperLog;
import com.ty.ssm.model.vo.SysOperLogQueryVo;

/**
 *
 * 操作日志接口
 *
 @author ty
 @create 2023-06-26-22:08  
 */

public interface ISysOperLogService {
	/**
	 * 保存操作日志
	 * @param sysLog
	 */
	public void saveSysLog(SysOperLog sysLog);

	/**
	 * 分页查询操作日志
	 * @param page
	 * @param limit
	 * @param sysOperLogQueryVo
	 * @return
	 */
	IPage<SysOperLog> operlogPageInfo(Long page, Long limit, SysOperLogQueryVo sysOperLogQueryVo);

	/**
	 * 根据id获取操作日志信息
	 * @param id
	 * @return
	 */
	SysOperLog getById(Long id);
}
