package com.ty.ssm.system.annotation;

import com.ty.ssm.system.enums.BusinessType;
import com.ty.ssm.system.enums.OperatorType;

import java.lang.annotation.*;

/**
 * 操作日志注解
 *
 @author ty
 @create 2023-06-26-21:53  
 */

@Target({ElementType.PARAMETER, ElementType.METHOD})//注解作用的地方(参数，方法)
@Retention(RetentionPolicy.RUNTIME)//注解作用范围(运行时)
@Documented//
public @interface Log {
	/**
	 * 模块
	 */
	public String title() default "";

	/**
	 * 功能
	 */
	public BusinessType businessType() default BusinessType.OTHER;

	/**
	 * 操作人类别
	 */
	public OperatorType operatorType() default OperatorType.MANAGE;

	/**
	 * 是否保存请求的参数
	 */
	public boolean isSaveRequestData() default true;

	/**
	 * 是否保存响应的参数
	 */
	public boolean isSaveResponseData() default true;
}
