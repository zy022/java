package com.ty.ssm.common.utils;

import io.jsonwebtoken.*;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 @author ty
 @create 2023-05-31-23:42  
 */

/**
 * 生成JSON Web令牌的工具类
 */
public class JwtHelper {

	//token过期时间
	private static long tokenExpiration = 365 * 24 * 60 * 60 * 1000;
	private static String tokenSignKey = "123456";

	/**
	 * 根据用户id和用户名称生成token字符串
	 * @param userId
	 * @param username
	 * @return
	 */
	public static String createToken(String userId, String username) {
		String token = Jwts.builder()
				.setSubject("AUTH-USER")//分组
				.setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))//设置过期时间
				.claim("userId", userId)//用户id
				.claim("username", username)//用户名
				.signWith(SignatureAlgorithm.HS512, tokenSignKey)//根据密钥加密
				.compressWith(CompressionCodecs.GZIP)//压缩
				.compact();
		return token;
	}

	public static String getUserId(String token) {
		try {
			if (StringUtils.isEmpty(token)) {
				return null;
			}
			Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
			Claims claims = claimsJws.getBody();
			String userId = (String) claims.get("userId");
			return userId;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String getUsername(String token) {
		try {
			if (StringUtils.isEmpty(token)) {
				return "";
			}

			Jws<Claims> claimsJws = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token);
			Claims claims = claimsJws.getBody();
			return (String) claims.get("username");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void removeToken(String token) {
		//jwttoken无需删除，客户端扔掉即可。
	}

	public static void main(String[] args) {
		String token = JwtHelper.createToken("1", "admin");
		System.out.println(token);
		System.out.println(JwtHelper.getUserId(token));
		System.out.println(JwtHelper.getUsername(token));
	}
}