package com.ty.ssm.common.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ty.ssm.common.result.Result;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 @author ty
 @create 2023-06-08-23:34  
 */

public class ResponseUtil {
	public static void out(HttpServletResponse response, Result r) {
		ObjectMapper mapper = new ObjectMapper();
		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		try {
			mapper.writeValue(response.getWriter(), r);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
